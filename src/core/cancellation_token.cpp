//
// Created by jamal on 26/05/2024.
//
#include <core/cancellation_token.h>
#include <core/logging.h>

using core::cancellation_token;

cancellation_token::cancellation_token() {
    token = default_state;
}

void cancellation_token::cancel() {
    token = false;
    sig();
}

bool cancellation_token::is_cancelled() const {
    return !token;
}

bool cancellation_token::is_active() const {
    return token;
}

void cancellation_token::subscribe(const std::function<void()> &cb) {
    BOOST_LOG_TRIVIAL(debug) << "connected a callback to signal";
    sig.connect(cb);
}


void cancellation_token::default_throw_on_cancel() {
    // subscribe to the signals
    subscribe([this]() {
        BOOST_LOG_TRIVIAL(trace) << "subscribe signal was triggered, calling `throw_exception`";
        // and throw an exception
        throw_exception();
    });

}

void cancellation_token::throw_if_cancelled() const {
    if (is_cancelled()) {
        throw_exception();
    }
}

void cancellation_token::throw_exception() const {
    throw operation_cancelled_exception();
}