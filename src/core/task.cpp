//
// Created by jamal on 28/09/2022.
//

#include <core/task.h>

#include <utility>

#include <utils/types.h>

namespace core {

    health::Status Task::health_check() const {
        return health::Status::Ok;
    }

    Task::RunOptions Task::setup(shared_ptr<IoContext> ctx, cancellation_token &token) {
        // the default execution strategy is to loop until the operation is requested to cancel.
        auto default_executor = [&](const RunOptions::ExecutorCallback &cb) {
            // connect to the token, so that when cancellation is request, the thread is blocked immediately.

            while (token.is_active()) {
                cb(token);
                token.throw_if_cancelled();
            }


        };
        return {
                default_executor
        };
    }

    bool Task::configure() {
        // default configure is true. (no configurations needed).
        return true;
    }

    std::string Task::name() const {
        return utils::type_name(this);
    }
}
