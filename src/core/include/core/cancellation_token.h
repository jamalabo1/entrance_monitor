//
// Created by jamal on 31/07/2022.
//

#ifndef ENTRANCE_MONITOR_V2_CANCELLATION_TOKEN_H
#define ENTRANCE_MONITOR_V2_CANCELLATION_TOKEN_H

#include <atomic>
#include <boost/signals2.hpp>
#include <core/logging.h>

namespace core {


    class operation_cancelled_exception : public std::exception {

    };

    /*!
     * @brief a mimic of the .net cancellation_token design pattern
     * */
    class cancellation_token {
    private:
        std::atomic_bool token;
        boost::signals2::signal<void()> sig;

    protected:
        boost::signals2::signal<void()> &get_signal() {
            return sig;
        }

    public:
        static constexpr bool default_state = true;

        cancellation_token();

        void cancel();

        bool is_cancelled() const;

        bool is_active() const;


        void subscribe(const std::function<void()>& cb);


        /*!
         * @brief subscribes to the token, and throws an exception on calling the token
         * */
        void default_throw_on_cancel();

        /*!
         * @brief in every "poll" the function checks if a request was made to cancel, if so, a custom exception is thrown.
         * @purpose to indicate the cooperative nature of the thread.
         * @throws operation_cancelled_exception
         * */
        void throw_if_cancelled() const;

        void throw_exception() const;
    };
}

#endif //ENTRANCE_MONITOR_V2_CANCELLATION_TOKEN_H
