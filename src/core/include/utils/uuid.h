//
// Created by jamal on 14/11/2022.
//

#ifndef ENTRANCE_MONITOR_V2_UUID_H
#define ENTRANCE_MONITOR_V2_UUID_H

#include <string>

namespace utils::uuid {
    std::string generateId();
}

#endif //ENTRANCE_MONITOR_V2_UUID_H
