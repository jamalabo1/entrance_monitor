//
// Created by root on 9/29/22.
//
#include <core/standalone.h>
#include <core/cancellation_token.h>
#include <boost/thread.hpp>
#include <utils/worker.h>

namespace core {
    using std::vector;
    using std::function;

    void init_service_runner() {
#ifdef WIN32
        BOOST_LOG_TRIVIAL(debug) << "[WIN32]: setting thread type to continous";
       // The following sets the appropriate flags to prevent system to go into sleep mode.
        SetThreadExecutionState(ES_CONTINUOUS | ES_SYSTEM_REQUIRED | ES_AWAYMODE_REQUIRED);
#endif


        boost::log::core::get()->set_filter
                (
                boost::log::trivial::severity >=
                #if APP_DEBUG
                boost::log::trivial::debug
#else
                boost::log::trivial::info
#endif
        );


    }


    void runner(fruit::Injector<IoContext> &injector) {
        BOOST_LOG_TRIVIAL(info) << "initiation service runner";
        init_service_runner();
        BOOST_LOG_TRIVIAL(info) << "Running as standalone";
        auto io_context = injector.get<shared_ptr<IoContext>>();
        BOOST_LOG_TRIVIAL(trace) << "getting `Service`(s) from injector";
        const vector<Service *> &services = injector.getMultibindings<Service>();
        run_services(io_context, services);
        BOOST_LOG_TRIVIAL(info) << "finished running standalone service";
    }


    void run_io_context(shared_ptr<core::IoContext> io_context) {
        boost::thread_group pool;

        auto hc = utils::worker::get_cores();

        BOOST_LOG_TRIVIAL(trace) << "device hardware concurrency: " << hc;

        for (auto i = 0u; i < hc; ++i)
            pool.create_thread([&] {
                io_context->run();
            });


        BOOST_LOG_TRIVIAL(trace) << "registering signals handler for termination";

        boost::asio::signal_set signals(GET_BOOST_IO_CONTEXT(io_context), SIGINT, SIGTERM);
        signals.async_wait(
                boost::bind(&boost::asio::io_service::stop, io_context->get_context()
                ));

        BOOST_LOG_TRIVIAL(debug) << "joining all threads (" << hc << ") for io_context.";

        pool.join_all();

        BOOST_LOG_TRIVIAL(debug) << "threads finished joining at ";
    }

    int run_services(shared_ptr<IoContext> io_context, const vector<Service *> &services) {

        auto post = [&](function<void()> &&func) {
            boost::asio::post(GET_BOOST_IO_CONTEXT(io_context), [func]() {
                DEFINE_STACK_TRACKED_CALL({
                                              func();
                                          })
            });
        };
        // create a token for the services
        // it's essentially useless since it's not used here.
        // TODO: implement a functionality to take use of cancellation tokens.
        cancellation_token token;

        for (const auto &service: services) {
            post([&post, &io_context, &token, service]() {
                BOOST_LOG_TRIVIAL(debug) << "setting up service: " << service->name();
                // setup service to register tasks.
                service->setup();

                // get registered tasks.
                auto tasks = service->getTasks();

                BOOST_LOG_TRIVIAL(debug) << "looping over service tasks";
                // loop over tasks
                for (const auto &task: tasks) {

                    // post each task to the io_context.
                    post([&io_context, &token, task]() {
                        auto task_name = task->name();


                        BOOST_LOG_TRIVIAL(debug) << "configuring task: " << task_name;
                        // configure the task.
                        bool result = task->configure();
                        BOOST_LOG_TRIVIAL(debug) << "task (" << task_name << ") configure returned : " << result;

                        if (!result) {
                            // TODO: handle the failing of the task (e.g., re-schedule executing (retry-policy)).
                        }

                        BOOST_LOG_TRIVIAL(debug) << "setting-up task: " << task_name;
                        // get runtime options for the task.
                        auto options = task->setup(io_context, token);

                        BOOST_LOG_TRIVIAL(debug) << "calling task (" << task_name << ") executor";
                        // execute the task, this call must be blocking or non-dependent on options
                        // because they are freed after the execution complete.
                        // only `task` lives.
                        options.executor([task, task_name](core::cancellation_token& token) {
                            //BOOST_LOG_TRIVIAL(info) << "is task " << task_name << " empty? == " << (task.get() == nullptr);
                            task->operator()(token);
                        });

                        BOOST_LOG_TRIVIAL(trace) << "execution of task: " << task_name << " has completed";
                    });
                }
            });
        }

        run_io_context(io_context);

        return -1;
    }
}