//
// Created by jamal on 19/09/2022.
//

#include <gtest/gtest.h>

#include <core/cancellation_token.h>
#include <core/logging.h>

#include <thread>

#include <boost/asio.hpp>

namespace {

    using namespace std;

    class CancellationTokenTestFixture : public ::testing::Test {

    };

    TEST(CancellationTokenTest, CancellationTokenCompliesWithDefault) {
        core::cancellation_token token;

        ASSERT_EQ(token.is_active(), core::cancellation_token::default_state);
    }

    TEST(CancellationTokenTest, CancellationTokenChangesStateOnCancel) {

        core::cancellation_token token;

        ASSERT_TRUE(token.is_active());

        token.cancel();

        EXPECT_FALSE(token.is_active());
        EXPECT_TRUE(token.is_cancelled());
    }


    TEST(CancellationTokenTest, CancellationTokenCanStopThread) {

        core::cancellation_token token;

        ASSERT_TRUE(token.is_active());

        atomic_bool is_test_func_running = false;
        auto test_func = [&]() {
            while (token.is_active()) {
                if (!is_test_func_running) {
                    is_test_func_running = true;
                }
            }
            is_test_func_running = false;
        };

        ASSERT_FALSE(is_test_func_running);


        boost::asio::thread_pool pool;
        boost::asio::post(pool, test_func);

        boost::asio::deadline_timer t(pool, boost::posix_time::seconds(5));

        t.async_wait([&](const boost::system::error_code &ec) {
            EXPECT_TRUE(is_test_func_running);
            token.cancel();
            pool.stop();
        });

        pool.join();

        EXPECT_TRUE(token.is_cancelled());
        EXPECT_FALSE(is_test_func_running);
    }

    TEST(CancellationTokenTest, CancellationTokenCanStopBlockingThread) {
        core::cancellation_token token;

        ASSERT_TRUE(token.is_active());

        boost::asio::thread_pool io(2);
        boost::asio::deadline_timer worker_timer(io);
        boost::asio::deadline_timer cancel_timer(io);
        auto worker_duration = boost::posix_time::seconds(10);
        worker_timer.expires_from_now(worker_duration);

        atomic_bool waiting_for_timer = false;
        atomic_bool changed_state = false;
        atomic_bool quited_while = false;

        boost::asio::post(io, [&]() {
            token.subscribe([&worker_timer] {
                BOOST_LOG_TRIVIAL(debug) << "calling worker_timer.cancel()";
                worker_timer.cancel();
            });
            if (token.is_active()) {
                waiting_for_timer = true;
                worker_timer.async_wait([&](const boost::system::error_code &ec) {
                    if (ec != boost::asio::error::operation_aborted) {
                        changed_state = true;
                    }
                });
            }
            quited_while = true;
        });

        // we can cancel the token here after running the task because it's not important how much the task lasted, what we want is to check if the while quits even before finishing the iteration.

        auto cancel_duration = worker_duration / 2;
        cancel_timer.expires_from_now(cancel_duration);
        cancel_timer.async_wait([&](const boost::system::error_code &ec) {
            BOOST_LOG_TRIVIAL(debug) << "calling token->cancel()";
            token.cancel();
        });

        io.join();

        EXPECT_TRUE(waiting_for_timer);
        EXPECT_TRUE(quited_while);
        EXPECT_FALSE(changed_state);

    }
}
